FROM docker.io/debian:buster-slim AS base

# Mastodon image for OpenShift Origin

ENV JE_VER=5.2.1 \
    MASTODON_VERS=3.4.3 \
    RUBY_VER=2.7.4

RUN set -x \
    && apt-get update \
    && apt -y install make autoconf gcc g++ build-essential wget git libpq-dev \
	bison libyaml-dev libgdbm-dev libreadline-dev libicu-dev libyaml-dev \
	libncurses5-dev libffi-dev zlib1g-dev libssl-dev binutils patch \
	libprotobuf-dev libidn11-dev libreadline-dev protobuf-compiler \
    && wget https://github.com/jemalloc/jemalloc/archive/$JE_VER.tar.gz \
	-O /usr/src/jemalloc.tar.gz \
    && tar -C /usr/src -xzf /usr/src/jemalloc.tar.gz \
    && rm -f /usr/src/jemalloc.tar.gz \
    && ( \
	cd /usr/src/jemalloc-$JE_VER \
	&& ./autogen.sh \
	&& ./configure --prefix=/opt/jemalloc \
	&& make -j$(getconf _NPROCESSORS_ONLN) \
	&& make install_bin install_include install_lib \
    ) \
    && ls /opt/jemalloc/lib | while read f; \
	do \
	    if test -e /usr/lib/$f; then \
		mv /usr/lib/$f /usr/lib/$f.orig; \
	    fi; \
	done \
    && ln -s /opt/jemalloc/lib/* /usr/lib/

ENV CPPFLAGS=-I/opt/jemalloc/include \
    LDFLAGS=-L/opt/jemalloc/lib/

RUN set -x \
    && wget -O /usr/src/ruby.tar.gz \
	https://cache.ruby-lang.org/pub/ruby/${RUBY_VER%.*}/ruby-$RUBY_VER.tar.gz \
    && tar -C /usr/src -xzf /usr/src/ruby.tar.gz \
    && rm -f /usr/src/ruby.tar.gz \
    && ( \
	cd /usr/src/ruby-$RUBY_VER \
	&& ./configure --prefix=/opt/ruby --with-jemalloc --with-shared \
	    --disable-install-doc \
	&& make -j$(getconf _NPROCESSORS_ONLN) \
	&& make install \
    )

ENV PATH=$PATH:/opt/ruby/bin

RUN set -x \
    && wget -O /usr/src/mastodon.tar.gz \
	https://github.com/tootsuite/mastodon/archive/v$MASTODON_VERS.tar.gz \
    && gem install bundler \
    && tar -C /usr/src -xzf /usr/src/mastodon.tar.gz \
    && rm -f /usr/src/mastodon.tar.gz \
    && ( \
	cd /usr/src/mastodon-$MASTODON_VERS \
	&& bundle config set deployment 'true' \
	&& bundle config set without 'development test' \
	&& bundle install -j$(getconf _NPROCESSORS_ONLN) \
    ) \
    && mv /usr/src/mastodon-$MASTODON_VERS /opt/mastodon

FROM opsperator/nodejs

ARG DO_UPGRADE=
ENV BIND=0.0.0.0 \
    MASTODON_VERS=3.4.3 \
    NODE_ENV=production \
    PATH=$PATH:/opt/ruby/bin:/opt/mastodon/bin \
    RAILS_ENV=production \
    RAILS_SERVE_STATIC_FILES=true

LABEL io.k8s.description="Mastodon Image." \
      io.k8s.display-name="Mastodon" \
      io.openshift.expose-services="8080:mastodon" \
      io.openshift.tags="mastodon" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mastodon" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MASTODON_VERS"

USER root

COPY --from=base /opt/jemalloc /opt/jemalloc
COPY --from=base /opt/mastodon /opt/mastodon
COPY --from=base /opt/ruby /opt/ruby
COPY config/* /

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Mastodon Dependencies" \
    && ls /opt/jemalloc/lib | while read f; \
	do \
	    if test -e /usr/lib/$f; then \
		mv /usr/lib/$f /usr/lib/$f.orig; \
	    fi; \
	done \
    && ln -s /opt/jemalloc/lib/* /usr/lib/ \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 /opt/mastodon/tmp \
    && apt-get install -y whois wget openssl postgresql-client imagemagick \
	ffmpeg libicu63 libprotobuf17 libidn11 libyaml-0-2 file tzdata \
	ca-certificates libreadline7 ldap-utils \
    && echo "# Install Mastodon" \
    && ( \
	cd /opt/mastodon \
	&& gem install bundler \
	&& yarn install --pure-lockfile \
	&& npx browserslist@latest --update-db \
	&& yarn cache clean \
    ) \
    && mv /reset-tls.sh /nsswrapper.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && chown -R 1001:root /opt/mastodon /etc/ssl \
	/usr/local/share/ca-certificates /opt/app-root/lib/pm2 \
    && chmod -R g=u /opt/mastodon /etc/ssl /usr/local/share/ca-certificates \
	/opt/app-root/lib/pm2 \
    && echo "# Cleaning Up" \
    && apt-get remove --purge -y git wget build-essential libssl-dev libpq-dev \
	libcurl4-openssl-dev libxml2-dev libxslt-dev libmagickwand-dev \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/opt/app-root/lib/pm2/.cache/ \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR /opt/mastodon
ENTRYPOINT ["dumb-init","--","/run-mastodon.sh"]
CMD [ "bundle", "exec", "rails", "s", "-p", "3000" ]
