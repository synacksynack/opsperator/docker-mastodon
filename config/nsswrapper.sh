#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/mastodon-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to mastodon
	(
	    cat /etc/group
	    echo "mastodon:x:`id -g`:"
	) >/tmp/mastodon-group
	(
	    cat /etc/passwd
	    echo "mastodon:x:`id -u`:`id -g`:mastodon:/mastodon:/bin/sh"
	) >/tmp/mastodon-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/mastodon-passwd
    export NSS_WRAPPER_GROUP=/tmp/mastodon-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
