#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh

ADMIN_PASSWORD="${ADMIN_PASSWORD:-}"
ADMIN_USER=${ADMIN_USER:-}
DB_HOST=${DB_HOST:-127.0.0.1}
DB_NAME=${DB_NAME:-mastodon}
DB_PASS=${DB_PASS:-secret}
DB_PORT=${DB_PORT:-5432}
DB_USER=${DB_USER:-mastodon}
LDAP_TLS_NO_VERIFY=1
LOCAL_DOMAIN=${LOCAL_DOMAIN:-demo.local}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=mastodon,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
REDIS_HOST=${REDIS_HOST:-127.0.0.1}
REDIS_PORT=${REDIS_PORT:-6379}

export LOCAL_DOMAIN REDIS_HOST REDIS_PORT
if test "$ADMIN_USER" -a -z "$ADMIN_EMAIL"; then
    ADMIN_EMAIL=$ADMIN_USER@$LOCAL_DOMAIN
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

echo Waiting for Postgres backend ...
cpt=0
waittime=5
while true
do
    if echo "$COMPONENT" | grep -E 'sidekiq|streaming' >/dev/null; then
	waittime=30
	if echo '\d' | PGPASSWORD="$DB_PASS" psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" 2>&1 \
		| grep web_settings >/dev/null; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 60; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
    else
	if echo '\d' | PGPASSWORD="$DB_PASS" psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
    fi
    sleep $waittime
    echo Postgres is ... KO
    cpt=`expr $cpt + 1`
done
export DB_HOST DB_NAME DB_PASS DB_PORT DB_USER

if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$OPENLDAP_PROTO" = ldaps; then
	if ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    LDAP_TLS_NO_VERIFY=0
	fi
    fi
    LDAP_ENABLED=true
    LDAP_HOST=$OPENLDAP_HOST
    LDAP_PORT=$OPENLDAP_PORT
    if test "$OPENLDAP_PROTO" = ldaps; then
	LDAP_METHOD=simple_tls
    else
	LDAP_METHOD=start_tls
    fi
    LDAP_BASE="$OPENLDAP_BASE"
    LDAP_BIND_DN="$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE"
    LDAP_PASSWORD="$OPENLDAP_BIND_PW"
    LDAP_UID=uid
    LDAP_MAIL=mail
    LDAP_SEARCH_FILTER="(&(|(%{uid}=%{email})(%{mail}=%{email}))(!(pwdAccountLockedTime=*)))"
    LDAP_UID_CONVERSION_ENABLED=true
    LDAP_UID_CONVERSION_SEARCH="., -"
    LDAP_UID_CONVERSION_REPLACE=_
    export LDAP_ENABLED LDAP_HOST LDAP_PORT LDAP_METHOD LDAP_BASE LDAP_BIND_DN \
	LDAP_PASSWORD LDAP_UID LDAP_SEARCH_FILTER LDAP_UID_CONVERSION_SEARCH \
	LDAP_MAIL LDAP_UID_CONVERSION_ENABLED LDAP_UID_CONVERSION_REPLACE
else
    unset LDAP_ENABLED LDAP_HOST LDAP_PORT LDAP_METHOD LDAP_BASE LDAP_BIND_DN \
	LDAP_PASSWORD LDAP_UID LDAP_SEARCH_FILTER LDAP_UID_CONVERSION_SEARCH \
	LDAP_MAIL LDAP_UID_CONVERSION_ENABLED LDAP_UID_CONVERSION_REPLACE
fi
unset OPENLDAP_HOST OPENLDAP_PORT OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW \
    OPENLDAP_BASE OPENLDAP_PROTO OPENLDAP_PORT

if test "$ES_HOST"; then
    ES_ENABLED=true
    ES_PORT=${ES_PORT:-9200}
    export ES_ENABLED ES_PORT
else
    unset ES_ENABLED
fi

if test -z "$SECRET_KEY_BASE"; then
    echo WARNING: generating SECRET_KEY_BASE, persisting deployments requires static secret
    SECRET_KEY_BASE=`rake secret`
    export SECRET_KEY_BASE
fi
if test -z "$OTP_SECRET"; then
    echo WARNING: generating OTP_SECRET, persisting deployments requires static secret
    OTP_SECRET=`rake secret`
    export OTP_SECRET
fi
if test -z "$VAPID_PRIVATE_KEY" -o -z "$VAPID_PUBLIC_KEY"; then
    echo WARNING: generating VAPID_PRIVATE_KEY and VAPID_PUBLIC_KEY, persisting deployments requires static keys
    eval `rake mastodon:webpush:generate_vapid_key`
    export VAPID_PRIVATE_KEY VAPID_PUBLIC_KEY
fi
if test "$SMTP_SERVER"; then
    SMTP_PORT=${SMTP_PORT:-25}
    if test -z "$SMTP_FROM_ADDRESS"; then
	SMTP_FROM_ADDRESS=noreply@$LOCAL_DOMAIN
    fi
    if test "$SMTP_PORT" = 465; then
	SMTP_OPENSSL_VERIFY_MODE=${SMTP_OPENSSL_VERIFY_MODE:-peer}
    else
	SMTP_OPENSSL_VERIFY_MODE=none
    fi
    if test "$SMTP_LOGIN" -a "$SMTP_PASSWORD"; then
	SMTP_AUTH_METHOD=plain
    else
	SMTP_AUTH_METHOD=none
    fi
    export SMTP_AUTH_METHOD SMTP_PORT SMTP_FROM_ADDRESS \
	SMTP_OPENSSL_VERIFY_MODE
fi

if test "$S3_HOST" -a "$AWS_ACCESS_KEY_ID" -a "$AWS_SECRET_ACCESS_KEY"; then
    S3_BUCKET=${S3_BUCKET:-mastodon}
    S3_OVERRIDE_PATH_STYLE=${S3_OVERRIDE_PATH_STYLE:-false}
    S3_PROTOCOL=${S3_PROTOCOL:-https}
    S3_REGION=${S3_REGION:-us-east-1}
    S3_SIGNATURE_VERSION=${S3_SIGNATURE_VERSION:v4}
    if test -z "$S3_PORT"; then
	if test "$S3_PROTOCOL" = https; then
	    S3_PORT=443
	else
	    S3_PORT=80
	fi
    fi
    S3_HOSTNAME=$S3_HOST
    S3_ENABLED=true
    S3_ENDPOINT=$S3_PROTOCOL://$S3_HOST:$S3_PORT
    export S3_ENABLED S3_ENDPOINT S3_BUCKET S3_HOSTNAME S3_PROTOCOL \
	S3_OVERRIDE_PATH_STYLE S3_SIGNATURE_VERSION
else
    unset S3_ENABLED AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
fi

/usr/local/bin/reset-tls.sh
case "$COMPONENT" in
    sidekiq)
	unset ADMIN_USER ADMIN_PASSWORD ADMIN_EMAIL
	if test "$BROWSERSLIST_UPDATE"; then
	    npx browserslist@latest --update-db
	fi
	sed -i 's|compile: false|compile: true|g' /opt/mastodon/config/webpacker.yml
	if ! ls /opt/mastodon/public/assets/pghero/*.png >/dev/null 2>&1; then
	    rails assets:precompile
	fi
	exec bundle exec sidekiq
	;;
    debug)
	sleep 86400
	;;
    streaming)
	unset ADMIN_USER ADMIN_PASSWORD ADMIN_EMAIL
	exec node ./streaming
	;;
    *)
	if test "$BROWSERSLIST_UPDATE"; then
	    npx browserslist@latest --update-db
	fi
	sed -i 's|compile: false|compile: true|g' /opt/mastodon/config/webpacker.yml
	if ! ls /opt/mastodon/public/assets/pghero/*.png >/dev/null 2>&1; then
	    rails assets:precompile
	fi
	rm -f ./tmp/pids/server.pid
	bundle exec rake db:migrate
	if test "$ADMIN_USER" -a "$ADMIN_PASSWORD"; then
	    if ! ./bin/tootctl media refresh --account $ADMIN_USER >/dev/null 2>&1; then
		echo NOTICE: Creating Initial Admin User
		bundle exec rails c <<EOR
account = Account.create!(username: "$ADMIN_USER")
user = User.create!(email: "$ADMIN_EMAIL", password: "$ADMIN_PASSWORD", agreement: 'true', account: account)
user.confirm
account.save!
user.save!
EOR
		./bin/tootctl accounts modify "$ADMIN_USER" --role admin
	    fi
	fi
	if test -s /show.html.haml; then
	    cat /show.html.haml >./app/views/about/show.html.haml
	fi
	unset ADMIN_USER ADMIN_PASSWORD ADMIN_EMAIL
	exec $@
	;;
esac
