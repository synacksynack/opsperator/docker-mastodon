SKIP_SQUASH?=1
IMAGE=opsperator/mastodon
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f redis || echo true
	@@docker run --name redis -p 6379:6379 -d docker.io/redis:6 || echo true
	@@docker rm -f postgres || echo true
	@@docker run --name postgres \
	    -e POSTGRESQL_USER=pguser \
	    -e POSTGRESQL_DATABASE=pgdb \
	    -e POSTGRESQL_PASSWORD=pgpassword \
	    -p 5432:5432 \
	    -d docker.io/centos/postgresql-12-centos7:latest
	@@sleep 10
	@@redisctn=`docker ps | awk '/redis/{print $$1;exit 0;}'`; \
	redisip=`docker inspect $$redisctn | awk '/"IPAddress": "/{print $$2;exit 0}' | cut -d'"' -f2`; \
	pgctn=`docker ps | awk '/postgres/{print $$1;exit 0;}'`; \
	pgip=`docker inspect $$pgctn | awk '/"IPAddress": "/{print $$2;exit 0}' | cut -d'"' -f2`; \
	( \
	    echo "REDIS_HOST=$$redisip"; \
	    echo "DB_NAME=pgdb"; \
	    echo "DB_USER=pguser"; \
	    echo "DB_PASS=pgpassword"; \
	    echo "DB_HOST=$$pgip"; \
	    echo "DEBUG=yay"; \
	    echo "SECRET_KEY_BASE=abcdefsecretkeybase123"; \
	    echo "OTP_SECRET=abcdefotpsecret123"; \
	    echo "VAPID_PUBLIC_KEY=BLmXnnuHAMOGt_att5hhzpS_Bl9cpItlPSMnVpp1jqYiaGjk2oi0Q-t5QktzqLNItWmkWgg1Z9-jEgPzh-iFaCU"; \
	    echo "VAPID_PRIVATE_KEY=NzW5acORSGDkITP9pc3uBOIozr5rXTqlYJ2xdSoa"; \
	) >.env
	@@docker rm -f mastodon-web || echo true
	@@docker run --name mastodon-web \
	    --env-file=./.env \
	    -e COMPONENT=web \
	    -p 3000:3000 \
	    --tmpfs /opt/mastodon/public/system/accounts:rw,size=65536k \
	    --tmpfs /opt/app-root/lib/pm2:rw,size=65536k \
	    -d $(IMAGE)
	@@docker rm -f mastodon-sidekiq || echo true
	@@docker run --name mastodon-sidekiq \
	    --env-file=./.env \
	    -e COMPONENT=sidekiq \
	    --tmpfs /opt/mastodon/public/system/accounts:rw,size=65536k \
	    --tmpfs /opt/app-root/lib/pm2:rw,size=65536k \
	    -d $(IMAGE)
	@@docker rm -f mastodon-streaming || echo true
	@@docker run --name mastodon-streaming \
	    --env-file=./.env \
	    -e COMPONENT=streaming \
	    -p 4000:4000 \
	    --tmpfs /opt/mastodon/public/system/accounts:rw,size=65536k \
	    --tmpfs /opt/app-root/lib/pm2:rw,size=65536k \
	    -d $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset secret deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "MASTODON_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "MASTODON_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
