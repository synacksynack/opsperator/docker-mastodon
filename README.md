# k8s Mastodon

|    Variable name               |    Description                      | Default                                                     |
| :----------------------------- | ----------------------------------- | ----------------------------------------------------------- |
|  `ADMIN_EMAIL`                 | Mastodon Admin Email                | `$ADMIN_USER@$LOCAL_DOMAIN`                                 |
|  `ADMIN_PASSWORD`              | Mastodon Admin Password             | undef                                                       |
|  `ADMIN_USER`                  | Mastodon Admin Username             | undef                                                       |
|  `AWS_ACCESS_KEY_ID`           | S3 ObjectStore Access Key           | undef                                                       |
|  `AWS_SECRET_ACCESS_KEY`       | S3 ObjectStore Secret Key           | undef                                                       |
|  `BROWSERSLIST_UPDATE`         | Updates Mastodon Browsers List      | undef                                                       |
|  `DB_HOST`                     | Mastodon Postgres Host              | `127.0.0.1`                                                 |
|  `DB_NAME`                     | Mastodon Postgres Database          | `mastodon`                                                  |
|  `DB_PASS`                     | Mastodon Postgres Password          | `secret`                                                    |
|  `DB_PORT`                     | Mastodon Postgres Port              | `5432`                                                      |
|  `DB_USER`                     | Mastodon Postgres Username          | `mastodon`                                                  |
|  `ES_HOST`                     | Mastodon ElasticSearch Host         | undef                                                       |
|  `ES_PORT`                     | Mastodon ElasticSearch Port         | `9200`                                                      |
|  `LOCAL_DOMAIN`                | Mastodon Instance Domain            | `demo.local`                                                |
|  `ONLY_TRUST_KUBE_CA`          | Don't trust base image CAs          | `false`, any other value disables ca-certificates CAs       |
|  `OPENLDAP_BASE`               | OpenLDAP Base                       | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix             | `cn=mastodon,ou=services`                                   |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password              | `secret`                                                    |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name                | `demo.local`                                                |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address            | undef                                                       |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                  | `389` or `636` depending on `OPENLDAP_PROTO`                |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                      | `ldap`                                                      |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass          | `inetOrgPerson`                                             |
|  `OTP_SECRET`                  | Mastodon OTP Secret                 | undef, generated during start-up, should be static          |
|  `REDIS_HOST`                  | Mastodon Redis Host                 | `127.0.0.1`                                                 |
|  `REDIS_PORT`                  | Mastodon Redis Port                 | `6379`                                                      |
|  `S3_BUCKET`                   | S3 ObjectStore Bucket               | `mastodon`                                                  |
|  `S3_HOST`                     | S3 ObjectStore Proxy Host           | undef                                                       |
|  `S3_PORT`                     | S3 ObjectStore Proxy Port           | `443` if proto https, `80` otherwise                        |
|  `S3_PROTOCOL`                 | S3 ObjectStore Proxy Proto          | `https`                                                     |
|  `S3_REGION`                   | S3 ObjectStore Region               | `us-east-1`                                                 |
|  `S3_SIGNATURE_VERSION`        | S3 ObjectStore Signatures Version   | `v4`                                                        |
|  `S3_OVERRIDE_PATH_STYLE`      | S3 ObjectStore Path Style           | `false`                                                     |
|  `SECRET_KEY_BASE`             | Mastodon Rails Secret Key           | undef, generated during start-up, should be static          |
|  `SMTP_FROM_ADDRESS`           | Mastodon SMTP From Address          | `noreply@$LOCAL_DOMAIN`                                     |
|  `SMTP_LOGIN`                  | Mastodon SMTP Auth Username         | undef                                                       |
|  `SMTP_OPENSSL_VERIFY_MODE`    | Mastodon SMTP TLS Verify            | `peer` if SMTP Port is 465, otherwise `none`                |
|  `SMTP_PASSWORD`               | Mastodon SMTP Auth Password         | undef                                                       |
|  `SMTP_PORT`                   | Mastodon SMTP Port                  | `25`                                                        |
|  `SMTP_SERVER`                 | Mastodon SMTP Relay                 | undef                                                       |
|  `VAPID_PRIVATE_KEY`           | Mastodon Vapid Private Key          | undef, generated during start-up, should be static          |
|  `VAPID_PUBLIC_KEY`            | Mastodon Vapid Public Key           | undef, generated during start-up, should be static          |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                     | Description                                 |
| :-------------------------------------- | ------------------------------------------- |
|  `/certs`                               | Mastodon Certificate Authorities (optional) |
|  `/opt/mastodon/public/system/accounts` | Mastodon Uploads directory                  |
